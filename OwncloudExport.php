<?php
/**
 * OwncloudExport class
 */

use Sabre\DAV\Client;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\Loader\ArrayLoader;

require_once __DIR__.'/vendor/autoload.php';

Yii::import('application.helpers.admin.export.*');

/**
 * Main plugin class
 */
class OwncloudExport extends PluginBase
{

    /**
     * Plugin description
     * @var string
     */
    static protected $description = 'Export responses to an ownCloud server';

    /**
     * Plugin name
     * @var string
     */
    static protected $name = 'ownCloud export';

    /**
     * Storage type to use
     * @var string
     */
    protected $storage = 'DbStorage';

    /**
     * Plugin settings
     * @var array
     */
    protected $settings = array(
         'owncloud_server' => array(
              'type' => 'string',
              'label' => 'ownCloud server'
         )
    );

    /**
     * Translator instance
     * @var Translator
     */
    private $translator;

    /**
     * Locales
     * @var array[]
     */
    static private $locales = [
        'en' => array(
            'owncloudPath' => 'ownCloud folder path (optional)',
            'owncloudPathPlaceholder' => 'folder/sub_folder/etc',
            'owncloudUser' => 'ownCloud user',
            'owncloudPassword' => 'ownCloud password'
        ),
        'fr' => array(
            'owncloudPath' => 'Chemin du dossier sur ownCloud (optionel)',
            'owncloudPathPlaceholder' => 'dossier/sous_dossier/etc',
            'owncloudUser' => 'Nom d\'utilisateur ownCloud',
            'owncloudPassword' => 'Mot de passe ownCloud'
        )
    ];

    /**
     * Initialize plugin
     * @return void
     */
    public function init()
    {
        $this->translator = new Translator(Yii::app()->getLanguage());
        $this->translator->setFallbackLocales(array('en'));
        $this->translator->addLoader('array', new ArrayLoader());

        foreach (self::$locales as $locale => $translations) {
            $this->translator->addResource('array', $translations, $locale);
        }

        $this->subscribe('afterSurveyComplete');
        $this->subscribe('beforeSurveySettings');
        $this->subscribe('newSurveySettings');
    }

    /**
     * Event called after the survey is completed
     * @return void
     */
    public function afterSurveyComplete()
    {
        $event = $this->getEvent();
        $responseId = $event->get('responseId');
        $surveyId = $event->get('surveyId');
        $server = $this->get('owncloud_server');
        $user = $this->get('owncloud_user', 'Survey', $surveyId);
        $password = $this->get('owncloud_password', 'Survey', $surveyId);

        if (isset($responseId) && $server && $user && $password) {
            $client = new Client(array(
                'baseUri' =>  $server.'/remote.php/webdav/'.
                    str_replace(' ', '%20', $this->get('owncloud_path', 'Survey', $surveyId)).'/',
                'userName' => $user,
                'password' => $password,
            ));

            $surveyDao = new SurveyDao();
            $survey = $surveyDao->loadSurveyById($surveyId, 'fr');
            $surveyDao->loadSurveyResults($survey, $responseId, $responseId);

            $tmpFile = tempnam(sys_get_temp_dir(), 'survey_'.$surveyId);
            $filename = str_replace(' ', '%20', $survey->languageSettings['surveyls_title'].'.xls');

            $response = $client->request('GET', $filename);
            if ($response['statusCode'] == 200) {
                file_put_contents($tmpFile, $response['body']);
                $objPHPExcel = PHPExcel_IOFactory::load($tmpFile);
                $sheet = $objPHPExcel->getActiveSheet();
            } else {
                $objPHPExcel = new PHPExcel();
                $sheet = $objPHPExcel->getActiveSheet();

                $row = 1;
                $col = 6;
                foreach ($survey->questions as $question) {
                    $sheet->setCellValueByColumnAndRow($col, $row, $question['question']);
                    $col++;
                }
            }

            foreach ($survey->responses as $response) {
                $row = $sheet->getHighestRow() + 1;
                $col = 0;
                foreach ($response as $i => $field) {
                    $sheet->setCellValueByColumnAndRow($col, $row, $field);
                    $col++;
                }
            }

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
            $objWriter->save($tmpFile);

            $client->request('PUT', $filename, file_get_contents($tmpFile));
        }
    }

    /**
     * Event used to add new survey settings
     * @return void
     */
    public function beforeSurveySettings()
    {
        $event = $this->getEvent();

        $event->set("surveysettings.{$this->id}", array(
                'name' => get_class($this),
                'settings' => array(
                    'owncloud_user' => array(
                        'type' => 'string',
                        'label' => $this->translator->trans('owncloudUser'),
                        'current' => $this->get('owncloud_user', 'Survey', $event->get('survey'))
                    ),
                    'owncloud_password' => array(
                        'type' => 'password',
                        'label' => $this->translator->trans('owncloudPassword'),
                        'current' => $this->get('owncloud_password', 'Survey', $event->get('survey'))
                    ),
                    'owncloud_path' => array(
                        'type' => 'string',
                        'label' => $this->translator->trans('owncloudPath'),
                        'htmlOptions' => ['placeholder' => $this->translator->trans('owncloudPathPlaceholder')],
                        'current' => $this->get('owncloud_path', 'Survey', $event->get('survey'))
                    )
                )
            ));
    }

    /**
     * Event called when survey settings are saved
     * @return void
     */
    public function newSurveySettings()
    {
        $event = $this->getEvent();
        $survey = $event->get('survey');

        foreach ($event->get('settings') as $name => $value) {
            $this->set($name, $value, 'Survey', $survey);
        }
    }
}
