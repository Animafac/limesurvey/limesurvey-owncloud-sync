# LimeSurvey ownCloud sync

LimeSurvey plugin that exports responses to an ownCloud server

## Install

You need to put the plugin in the `plugins/` folder of your LimeSurvey instance and then enable it in the LimeSurvey admin.

## Setup

You need to install dependencies with [Composer](https://getcomposer.org/):

```bash
composer install
```

## Usage

When editing a survey, you can go in the plugin settings (at the bottom of the survey settings menu) and fill in your ownCloud credentials and a path to an existing ownCloud folder.
The plugin will then create an Excel file in that folder and update it with new responses.

**Warning**: you should use an application password instead of your real password.

## How it works

The plugin uses the `afterSurveyComplete` hook to be triggered when a new response is complete and then uses WebDAV to connect to ownCloud and PHPExcel to create/edit the spreadsheet.
